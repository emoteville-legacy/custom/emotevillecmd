![](https://assets.emo.gg/logo/emoteville-gold3-small.png)
<br>
[Website](https://go.emo.gg/) - [Discord](https://go.emo.gg/discord) - [Issue Tracker](https://go.emo.gg/issue-tracker)

<br>
EmoteVilleCMD is a spigot plugin that allows plugins to execute multiple commands in different conditions.

The original documentation for this project is located [HERE](https://www.evernote.com/shard/s627/sh/ec9c9da0-55fe-4c1e-8d1c-3587ebf643b4/9308846b4e634e28c49c628c4ebe12b0).

It is possible to interact with other plugins using the commands and placeholders as an input/output system, this is how most of our plugins work.


**_Commands_**

| Command | Description |
| ------ | ------ |
| /cmd (file) (player)  | Execute a file for the player. |
| /cmd reload | Reload all configurations. |
