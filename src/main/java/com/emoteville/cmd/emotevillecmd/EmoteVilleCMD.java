package com.emoteville.cmd.emotevillecmd;

import com.emoteville.cmd.emotevillecmd.cmd.CmdManager;
import com.emoteville.cmd.emotevillecmd.cmd.OptionsManager;
import com.emoteville.cmd.emotevillecmd.commands.CmdCommand;
import com.emoteville.cmd.emotevillecmd.commands.TellCommand;
import org.bukkit.plugin.java.JavaPlugin;

public final class EmoteVilleCMD extends JavaPlugin {

    private CmdManager cmdManager;
    private OptionsManager optionsManager;

    @Override
    public void onEnable() {
        cmdManager = new CmdManager(this);
        optionsManager = new OptionsManager(this);

        getCommand("cmd").setExecutor(new CmdCommand(this));
        getCommand("tell").setExecutor(new TellCommand());
    }

    @Override
    public void onDisable() {
        optionsManager.save();
    }

    public CmdManager getCmdManager() {
        return cmdManager;
    }

    public OptionsManager getOptionsManager() {
        return optionsManager;
    }
}
