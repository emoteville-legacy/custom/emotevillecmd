package com.emoteville.cmd.emotevillecmd.cmd;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cmd {

    private final String alias;
    private final Map<String, Object> options;
    private final List<String> rawCommands;

    public Cmd(String alias, Map<String, Object> options, List<String> rawCommands) {
        if (options == null) options = new HashMap<>();

        this.alias = alias;
        this.options = options;
        this.rawCommands = rawCommands;
    }

    public void execute(Player player) {
        rawCommands.forEach(rawCommand -> {
            String command = PlaceholderAPI.setPlaceholders(player, rawCommand).replace("{player}", player.getName());
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
        });
    }

    public String getAlias() {
        return alias;
    }

    public Map<String, Object> getOptions() {
        return options;
    }
}
