package com.emoteville.cmd.emotevillecmd.cmd;

import com.emoteville.cmd.emotevillecmd.EmoteVilleCMD;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.bukkit.event.Listener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CmdManager implements Listener {

    private final EmoteVilleCMD plugin;

    private final List<Cmd> cmdList = new ArrayList<>();

    public CmdManager(EmoteVilleCMD plugin) {
        this.plugin = plugin;

        loadCmds();
    }

    public void loadCmds() {
        File cmdDir = new File(plugin.getDataFolder() + "/cmd/");
        cmdDir.mkdirs();

        for (File file : cmdDir.listFiles()) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Map<String, Object> map = new HashMap<>();
            try {
                map = gson.fromJson(new FileReader(file), HashMap.class);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            String alias = file.getName().replace(".json", "");
            Map<String, Object> options = (Map<String, Object>) map.get("options");
            List<String> commands = (List<String>) map.get("commands");

            cmdList.add(new Cmd(alias, options, commands));
        }
    }

    public Cmd getCmd(String alias) {
        for (Cmd cmd : cmdList) {
            if (cmd.getAlias().equalsIgnoreCase(alias)) {
                return cmd;
            }
        }
        return null;
    }

    public List<Cmd> getCmdList() {
        return cmdList;
    }
}
