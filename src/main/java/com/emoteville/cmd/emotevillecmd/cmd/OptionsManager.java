package com.emoteville.cmd.emotevillecmd.cmd;

import com.emoteville.cmd.emotevillecmd.EmoteVilleCMD;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class OptionsManager implements Listener {

    private final EmoteVilleCMD plugin;

    private final FileConfiguration playersConfig;
    private final File playersFile;

    public OptionsManager(EmoteVilleCMD plugin) {
        this.plugin = plugin;

        playersFile = new File(plugin.getDataFolder(), "players.yml");
        if (!playersFile.exists()) {
            try {
                playersFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        playersConfig = YamlConfiguration.loadConfiguration(playersFile);

        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public void save() {
        try {
            playersConfig.save(playersFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        for (Cmd cmd : plugin.getCmdManager().getCmdList()) {
            String playerUUID = player.getUniqueId().toString();
            Map<String, Object> options = cmd.getOptions();

            if (options.get("limit") != null) {
                if (playersConfig.getInt(playerUUID + "." + cmd.getAlias() + ".counter") >= ((Number) options.get("limit")).longValue()) {
                    return;
                } else {
                    playersConfig.set(playerUUID + "." + cmd.getAlias() + ".counter", playersConfig.getInt(playerUUID + "." + cmd.getAlias() + ".counter") + 1);
                }
            }

            if (!player.hasPlayedBefore() && options.get("first-join") != null && options.get("first-join").equals(true)
                    || (options.get("welcome-back") != null && player.getLastPlayed() > Long.parseLong((String) options.get("welcome-back")))) {
                cmd.execute(player);
            }
        }
    }
}
