package com.emoteville.cmd.emotevillecmd.commands;

import com.emoteville.cmd.emotevillecmd.EmoteVilleCMD;
import com.emoteville.cmd.emotevillecmd.cmd.Cmd;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class CmdCommand implements CommandExecutor {

    private final EmoteVilleCMD plugin;

    public CmdCommand(EmoteVilleCMD plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
            plugin.getCmdManager().loadCmds();
            sender.sendMessage(ChatColor.GREEN + "Reloaded cmds");
            return true;
        } else if (args.length > 1) {
            Cmd cmd = plugin.getCmdManager().getCmd(args[0].toLowerCase());
            Player player = Bukkit.getPlayer(args[1]);

            if (cmd == null) {
                sender.sendMessage(ChatColor.RED + "Could not find cmd");
            } else if (player == null) {
                sender.sendMessage(ChatColor.RED + "Could not find player");
            } else {
                cmd.execute(player);
                sender.sendMessage(ChatColor.GREEN + cmd.getAlias() + " has been executed for " + player.getName());
            }
            return true;
        }
        return false;
    }
}
