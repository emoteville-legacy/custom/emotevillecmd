package com.emoteville.cmd.emotevillecmd.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.stream.IntStream;

public class TellCommand implements CommandExecutor {

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length > 1) {
            Player player = Bukkit.getPlayer(args[0]);
            if (player == null) {
                sender.sendMessage(ChatColor.RED + "Could not find player");
            } else {
                String[] result = new String[args.length - 1];
                IntStream.range(0, args.length).filter(i -> i != 0).forEach(i -> result[i - 1] = args[i]);

                player.sendMessage(String.join(" ", result));
            }
            return true;
        }
        return false;
    }
}
